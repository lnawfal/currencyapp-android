package com.adria.adriatestapp;

/**
 * Created by LAKCHOUCH NAOUFAL on 20/08/20.
 */

public class Config {

    //General URLs used by the app to load Exchange Rate Data
    private static final String EXCHANGE_RATE_BASE_URL    = "https://api.exchangeratesapi.io/";
    public static final String EXCHANGE_RATE_LATEST_URL   =  EXCHANGE_RATE_BASE_URL + "latest?base="  ;
    public static final String EXCHANGE_RATE_HISTORY_URL  =  EXCHANGE_RATE_BASE_URL + "history?base="  ;

    //The necessary parameters to construct the Exchange Rates History URL
    public static final String EXCHANGE_RATE_START_DATE_PARAM  =  "&start_at=" ;
    public static final String EXCHANGE_RATE_END_DATE_PARAM    =  "&end_at=" ;
    public static final String EXCHANGE_RATE_SYMBOL_PARAM     =  "&symbols=" ;

    //The SharedPreferences Keys
    public static final String PREFERENCE_NAME      = "currency_preference";
    public static final String PREFERENCE_CURRENCY  = "pr_currency";
    public static final String PREFERENCE_POSITION  = "pr_position";
}
