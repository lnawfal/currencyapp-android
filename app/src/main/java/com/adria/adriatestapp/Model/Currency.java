package com.adria.adriatestapp.Model;

import androidx.annotation.NonNull;

import java.io.Serializable;

/**
 * Created by LAKCHOUCH NAOUFAL on 20/08/20.
 */

public class Currency implements Serializable {

    private String abbreviation;
    private double value;

    public Currency() {}

    public Currency(String abbreviation, double value) {
        this.abbreviation = abbreviation;
        this.value = value;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @NonNull
    public String toString() {
        return "[abbreviation=" + abbreviation + ", value=" + value + " ]";
    }
}
