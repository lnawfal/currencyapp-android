package com.adria.adriatestapp;

import android.content.pm.ActivityInfo;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.view.Menu;
import android.view.MenuItem;

import com.adria.adriatestapp.Model.Currency;
import com.adria.adriatestapp.ui.fragments.CurrencyHistoryFragment;
import com.adria.adriatestapp.ui.fragments.CurrencyListFragment;
import com.adria.adriatestapp.utils.FireBaseAnalyticsTracker;
import com.adria.adriatestapp.utils.UIUtils;

/**
 * Activity which displays different Exchange Rates by currency and the two months history
 * via {@link CurrencyListFragment} and {CurrencyHistoryFragment}.
 */

@SuppressWarnings("AccessStaticViaInstance")
public class CurrencyActivity extends AppCompatActivity {

    private final UIUtils uiUtils =  new UIUtils();

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPanel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Tracking the screen name using FireBaseAnalytics
        FireBaseAnalyticsTracker.trackScreen(this, CurrencyActivity.class.getName());

        //Forcing the 
        if(uiUtils.isTablet(this)){
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

        if (findViewById(R.id.fragment_detail_container) != null) {
            mTwoPanel = true;

            Currency currency0 = new Currency("AUD",0);

            Fragment HistoryFragment = new CurrencyHistoryFragment().newInstance(currency0,mTwoPanel);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_detail_container, HistoryFragment, "HistoryFragment")
                    .commitAllowingStateLoss();
            getSupportFragmentManager().executePendingTransactions();
        }

        //Creating and adding the CurrencyListFragment to CurrencyActivity
        Fragment currencyFragment = new CurrencyListFragment().newInstance(mTwoPanel);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, currencyFragment, "CurrencyFragment")
                .commitAllowingStateLoss();
        getSupportFragmentManager().executePendingTransactions();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}