package com.adria.adriatestapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adria.adriatestapp.Model.Currency;
import com.adria.adriatestapp.R;
import com.adria.adriatestapp.ui.fragments.CurrencyHistoryFragment;

import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by LAKCHOUCH NAOUFAL on 20/08/20.
 */

public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyAdapter.MyViewHolder> {

    private final Context mContext;
    private final List<Currency> currencyList;
    private final boolean mTwoPanel;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public final TextView title;
        public final TextView count;
        public final View mView;

        public MyViewHolder(View view) {
            super(view);

            mView    = view;
            title    = (TextView) view.findViewById(R.id.title);
            count    = (TextView) view.findViewById(R.id.value);

            mView.setOnClickListener(new View.OnClickListener() {
                @SuppressWarnings("AccessStaticViaInstance")
                @Override public void onClick(View v) {
                    // item clicked
                    FragmentManager fragmentManager = ((AppCompatActivity)mContext).getSupportFragmentManager();
                    Fragment newFragment = new CurrencyHistoryFragment().newInstance(currencyList.get((int)mView.getTag()),mTwoPanel);

                    if(mTwoPanel){
                        fragmentManager.beginTransaction()
                                       .replace(R.id.fragment_detail_container, newFragment, "HistoryFragment")
                                       .commitAllowingStateLoss();
                    }else{
                        fragmentManager.beginTransaction()
                                       .add(R.id.fragment_container, newFragment, "HistoryFragment")
                                       .addToBackStack("HistoryFragment")
                                       .commitAllowingStateLoss();
                    }
                    fragmentManager.executePendingTransactions();
                }
            });
        }
    }


    public CurrencyAdapter(Context mContext, List<Currency> currencyList, boolean mTwoPanel) {
        this.mContext = mContext;
        this.currencyList = currencyList;
        this.mTwoPanel = mTwoPanel;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.currency_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Currency currency = currencyList.get(position);
        holder.mView.setTag(position);
        holder.title.setText(currency.getAbbreviation());
        holder.count.setText(String.format(Locale.ENGLISH,"%f",currency.getValue()));
    }

    @Override
    public int getItemCount() {
        return currencyList.size();
    }
}
