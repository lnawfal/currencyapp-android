package com.adria.adriatestapp.ui.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.adria.adriatestapp.Config;
import com.adria.adriatestapp.Model.Currency;
import com.adria.adriatestapp.R;
import com.adria.adriatestapp.adapter.CurrencyAdapter;
import com.adria.adriatestapp.utils.AdriaTestUtils;
import com.adria.adriatestapp.utils.FireBaseAnalyticsTracker;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static com.adria.adriatestapp.Config.PREFERENCE_NAME;
import static com.adria.adriatestapp.Config.PREFERENCE_CURRENCY;
import static com.adria.adriatestapp.Config.PREFERENCE_POSITION;


/**
 * Created by LAKCHOUCH NAOUFAL on 20/08/20.
 */

public class CurrencyListFragment extends Fragment implements AdapterView.OnItemSelectedListener{

    private CurrencyAdapter adapter;
    private List<Currency> currencyList;

    private final AdriaTestUtils Utils = new AdriaTestUtils() ;
    private ExchangeTask mExchangeTask = new ExchangeTask();

    private ProgressBar progressBar ;
    private RecyclerView recyclerView;
    private RelativeLayout reloadLayout;
    private TextView reloadTV;

    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;

    private boolean twoPanel;

    private final String[] supportedCurrency = {"AUD","BGN","BRL","CAD","CHF","CNY","CZK","DKK","EUR","GBP","HKD","HRK","HUF","IDR","ILS","INR","ISK","JPY",
            "KRW","MXN","MYR","NOK","NZD","PLN","RON","RUB","SEK","SGD","THB","TRY","USD","ZAR"};

    public static CurrencyListFragment newInstance(boolean mTwoPanel) {
        CurrencyListFragment frag = new CurrencyListFragment();
        Bundle args = new Bundle();
        args.putBoolean("param",mTwoPanel);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
        twoPanel = getArguments().getBoolean("param");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View mView = inflater.inflate(R.layout.fragment_currency_list, container, false);

        recyclerView = (RecyclerView)mView.findViewById(R.id.recycler_view);
        progressBar  = (ProgressBar)mView.findViewById(R.id.progressBar1);
        reloadLayout = (RelativeLayout)mView.findViewById(R.id.reload_container);
        reloadTV     = (TextView)mView.findViewById(R.id.reload_text);

        Spinner spinner     = (Spinner)mView.findViewById(R.id.spinner);
        Button reloadButton = (Button)mView.findViewById(R.id.reload_btn);

        if(getActivity() != null){
        prefs = getActivity().getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        editor = prefs.edit();
        }

        // Creating adapter for spinner
        ArrayAdapter<CharSequence> dataAdapter = new ArrayAdapter<CharSequence>(getActivity(), android.R.layout.simple_spinner_item, supportedCurrency);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
        spinner.setSelection(prefs.getInt(PREFERENCE_POSITION,8));
        spinner.setOnItemSelectedListener(this);

        currencyList = new ArrayList<>();
        adapter = new CurrencyAdapter(getActivity(), currencyList, twoPanel);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        mExchangeTask = new ExchangeTask();
        mExchangeTask.execute(Config.EXCHANGE_RATE_LATEST_URL + prefs.getString(PREFERENCE_CURRENCY,"EUR"));

        reloadButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                if(!mExchangeTask.getStatus().equals(AsyncTask.Status.RUNNING)){
                    mExchangeTask = new ExchangeTask();
                    mExchangeTask.execute(Config.EXCHANGE_RATE_LATEST_URL + prefs.getString(PREFERENCE_CURRENCY,"EUR"));
                }
            }
        });

        FireBaseAnalyticsTracker.trackScreen(getActivity(), CurrencyListFragment.class.getName());

        return mView;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(mExchangeTask.getStatus().equals(AsyncTask.Status.RUNNING)){
            mExchangeTask.cancel(true);
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        // On selecting a spinner item
        String pCurrency = adapterView.getItemAtPosition(position).toString();

        editor.putString(PREFERENCE_CURRENCY, pCurrency);
        editor.putInt(PREFERENCE_POSITION,position);
        editor.apply();

        if(!mExchangeTask.getStatus().equals(AsyncTask.Status.RUNNING)){
            mExchangeTask = new ExchangeTask();
            mExchangeTask.execute(Config.EXCHANGE_RATE_LATEST_URL + pCurrency);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @SuppressWarnings("deprecation")
    private class ExchangeTask extends AsyncTask<String, Void, Void> {

        String ServerResponse  = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressBar.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.INVISIBLE);
            reloadLayout.setVisibility(View.GONE);
        }

        @Override
        protected Void doInBackground(String... params) {

            try {
                ServerResponse = Utils.readFromUrl(params[0]);
                Log.v("ExchangeRate API: ", ServerResponse);

                if(!ServerResponse.isEmpty()){
                JSONObject ExchangeRateObject = new JSONObject(ServerResponse);
                JSONObject ratesObject =  ExchangeRateObject.getJSONObject("rates");
                currencyList.clear();

                for (String currencyS : supportedCurrency) {
                    if(ratesObject.has(currencyS) && ratesObject.getDouble(currencyS) != 1){
                    Currency currency = new Currency();
                    currency.setAbbreviation(currencyS);
                    currency.setValue(ratesObject.getDouble(currencyS));
                    currencyList.add(currency);
                    }
                }
                }

            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            progressBar.setVisibility(View.INVISIBLE);

            if(!ServerResponse.isEmpty() && currencyList.size() > 0){
                recyclerView.setVisibility(View.VISIBLE);
                adapter.notifyDataSetChanged();
            }else{
                reloadLayout.setVisibility(View.VISIBLE);
                if(!Utils.isNetworkConnected(getActivity())){
                    reloadTV.setText(getActivity().getResources().getString(R.string.network_error_msg));
                }else{
                    reloadTV.setText(getActivity().getResources().getString(R.string.refresh_text));
                }
            }
        }

    }
}