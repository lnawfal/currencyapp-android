package com.adria.adriatestapp.ui.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.adria.adriatestapp.Config;
import com.adria.adriatestapp.Model.Currency;
import com.adria.adriatestapp.R;
import com.adria.adriatestapp.utils.AdriaTestUtils;
import com.adria.adriatestapp.utils.MyMarkerView;

import com.adria.adriatestapp.utils.FireBaseAnalyticsTracker;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IFillFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import static com.adria.adriatestapp.Config.PREFERENCE_CURRENCY;
import static com.adria.adriatestapp.Config.PREFERENCE_NAME;

/**
 * Created by LAKCHOUCH NAOUFAL on 21/08/20.
 */

public class CurrencyHistoryFragment extends Fragment {

    private final AdriaTestUtils Utils = new AdriaTestUtils() ;
    private ExchangeHistoryTask mExchangeHistoryTask = new ExchangeHistoryTask();

    private ProgressBar progressBar ;
    private LineChart mLineChart;
    private RelativeLayout reloadLayout;
    private TextView reloadTV;

    private String symbol = "";
    private boolean mTwoPanel;
    private Currency currency =  new Currency();

    private SharedPreferences prefs;

    public static CurrencyHistoryFragment newInstance(Currency currency, boolean mTwoPanel) {
        CurrencyHistoryFragment frag = new CurrencyHistoryFragment();
        Bundle args = new Bundle();
        args.putBoolean("param",mTwoPanel);
        args.putSerializable("currency", currency);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Getting the Currency Param passed by CurrencyListFragment
       if(getArguments() != null){
        currency  = (Currency)getArguments().getSerializable("currency");
        mTwoPanel = getArguments().getBoolean("param");
        symbol    = currency.getAbbreviation();
       }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View mView  = inflater.inflate(R.layout.fragment_history, container, false);
        progressBar  = (ProgressBar)mView.findViewById(R.id.progressBar2);
        mLineChart   = (LineChart)mView.findViewById(R.id.chart1);
        reloadLayout = (RelativeLayout)mView.findViewById(R.id.reload_container);
        reloadTV     = (TextView)mView.findViewById(R.id.reload_text);

        Button reloadButton = (Button)mView.findViewById(R.id.reload_btn);
        TextView currenciesTV = (TextView)mView.findViewById(R.id.currencies);

        //Getting the app SharedPreferences
        if(getActivity() != null)
        prefs = getActivity().getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);

        //Constructing the currencies label (Ex: 1 EUR   >> GBP)
        currenciesTV.setText(String.format("1 %s      >>      %s", prefs.getString(PREFERENCE_CURRENCY,"EUR"), currency.getAbbreviation()));

        //Reloading Data after a loading failure
        reloadButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                if(!mExchangeHistoryTask.getStatus().equals(AsyncTask.Status.RUNNING)){
                    mExchangeHistoryTask = new ExchangeHistoryTask();
                    mExchangeHistoryTask.execute(ConstructExchangeHistoryUrl(symbol));
                }
            }
        });

        //Tracking the screen name using FireBaseAnalytics
        FireBaseAnalyticsTracker.trackScreen(getActivity(), CurrencyHistoryFragment.class.getName());

        return mView;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(!mTwoPanel){
           HideActionBar();
        }

        createLineChart();

        //Getting the Exchange Rate History Data
        mExchangeHistoryTask = new ExchangeHistoryTask();
        mExchangeHistoryTask.execute(ConstructExchangeHistoryUrl(symbol));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if(!mTwoPanel){
           ShowActionBar();
        }

        if(mExchangeHistoryTask.getStatus().equals(AsyncTask.Status.RUNNING)){
            mExchangeHistoryTask.cancel(true);
        }
    }

    //Getting History data by using AsyncTask to load data in Background (Recommended by Android Developer Team)
    @SuppressWarnings("deprecation")
    private class ExchangeHistoryTask extends AsyncTask<String, Void, Void> {

        String ServerResponse  = "";
        final List<String> usedDates = new ArrayList<>();
        final List<Double> values = new ArrayList<>();

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            mLineChart.setVisibility(View.INVISIBLE);
            reloadLayout.setVisibility(View.GONE);
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... params) {

            try {
                ServerResponse = Utils.readFromUrl(params[0]);
                Log.v("ExchangeRate API: ", ServerResponse);

                if(!ServerResponse.isEmpty()){
                JSONObject ExchangeRateObject = new JSONObject(ServerResponse);
                JSONObject ratesObject =  ExchangeRateObject.getJSONObject("rates");

                List<String> dates = Utils.getDatesList() ;

                for (int i = 0; i < dates.size(); i++) {
                    if(ratesObject.has(dates.get(i))){
                    JSONObject currencyObject = ratesObject.getJSONObject(dates.get(i));
                    values.add(currencyObject.getDouble(symbol));
                    // if(i%3 == 0){
                    usedDates.add(dates.get(i));
                    //}
                    }
                }
                }

            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            progressBar.setVisibility(View.INVISIBLE);
            if(!ServerResponse.isEmpty() && usedDates.size() > 0){
               mLineChart.setVisibility(View.VISIBLE);
               setData(usedDates,values);
            }else{
                reloadLayout.setVisibility(View.VISIBLE);
                if(!Utils.isNetworkConnected(getActivity())){
                    reloadTV.setText(getActivity().getResources().getString(R.string.network_error_msg));
                }else{
                    reloadTV.setText(getActivity().getResources().getString(R.string.refresh_text));
                }
            }

        }

    }

    //This method is used to build the ExchangeRateHistory URL
    private String ConstructExchangeHistoryUrl(String symbol){
        return String.format("%s%s%s%s%s%s%s%s",Config.EXCHANGE_RATE_HISTORY_URL, prefs.getString(PREFERENCE_CURRENCY,"EUR"),
                Config.EXCHANGE_RATE_START_DATE_PARAM,  Utils.getStartDate(),
                Config.EXCHANGE_RATE_END_DATE_PARAM,  Utils.getEndDate(),
                Config.EXCHANGE_RATE_SYMBOL_PARAM, symbol);
    }

    private void HideActionBar() {
        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        if (actionBar == null) {
            return;
        }
        actionBar.hide();
    }

    private void ShowActionBar() {
        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        if (actionBar == null) {
            return;
        }
        actionBar.show();
    }


    private void createLineChart() {

        mLineChart.setDragDecelerationFrictionCoef(0.9f);

        // set an alternative background color
        mLineChart.setBackgroundColor(Color.TRANSPARENT);
        mLineChart.getDescription().setEnabled(false);
        mLineChart.setTouchEnabled(true);
        mLineChart.setDrawGridBackground(false);

        // enable scaling and dragging
        mLineChart.setDragEnabled(true);
        mLineChart.setScaleEnabled(true);
        mLineChart.setHighlightPerDragEnabled(true);

        // force pinch zoom along both axis
        mLineChart.setPinchZoom(true);

        MarkerView mv = new MyMarkerView(getActivity(), R.layout.markerview);
        mv.setChartView(mLineChart); // For bounds control
        mLineChart.setMarker(mv);

        XAxis xAxis;
        xAxis = mLineChart.getXAxis();
        xAxis.enableGridDashedLine(10f, 10f, 0f);
        xAxis.setTextColor(Color.WHITE);

        YAxis yAxis;
        yAxis = mLineChart.getAxisLeft();
        // disable dual axis (only use LEFT axis)
        mLineChart.getAxisRight().setEnabled(false);
        // horizontal grid lines
        yAxis.enableGridDashedLine(10f, 10f, 0f);
        yAxis.setTextColor(Color.WHITE);

        // draw points over time
        mLineChart.animateX(1500);
        // get the legend (only possible after setting data)
        Legend l = mLineChart.getLegend();
        // draw legend entries as lines
        l.setForm(Legend.LegendForm.LINE);
        l.setTextSize(11f);
        l.setTextColor(Color.WHITE);
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
    }

    private void setData(List<String> usedDates, List<Double> currencies) {

        ArrayList<Entry> values = new ArrayList<>();
        double maxValue, minValue, half_diff;

        maxValue = Collections.max(currencies);
        minValue = Collections.min(currencies);
        half_diff = (maxValue - minValue)/4 ;

        Log.v("Max value ", "" + maxValue);
        Log.v("Min value", "" + minValue);
        Log.v("Diff", "" + half_diff);
        Log.v("dates size", "" + usedDates.size());

        for (int i = 0; i < usedDates.size(); i++) {
            double d = currencies.get(i);
            float val = (float)d;
            values.add(new Entry(i, val));
        }

        YAxis yAxis = mLineChart.getAxisLeft();

        if(half_diff > 0){
            yAxis.setAxisMaximum((float)maxValue + (float)half_diff);
            yAxis.setAxisMinimum((float)minValue - (float)half_diff);
        }else{
            yAxis.setAxisMaximum((float)maxValue + 1f);
            yAxis.setAxisMinimum((float)minValue - 1f);
        }

        XAxis xAxis = mLineChart.getXAxis();
        xAxis.setLabelRotationAngle(270);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);


        mLineChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(usedDates));

        LineDataSet set1;

        if (mLineChart.getData() != null && mLineChart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) mLineChart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            set1.notifyDataSetChanged();
            mLineChart.getData().notifyDataChanged();
            mLineChart.notifyDataSetChanged();
        } else {
            // create a dataset and give it a type
            set1 = new LineDataSet(values, "Dates");
            set1.setDrawIcons(false);
            // draw dashed line
           // set1.enableDashedLine(10f, 5f, 0f);

            // line thickness and point size
            set1.setLineWidth(3f);
            set1.setCircleRadius(3f);

            // draw points as solid circles
            set1.setDrawCircleHole(false);

            // customize legend entry
            set1.setFormLineWidth(1f);
            set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            set1.setFormSize(15.f);

            // text size of values
            set1.setValueTextSize(9f);

            // draw selection line as dashed
            set1.enableDashedHighlightLine(10f, 5f, 0f);

            set1.setColor(Color.parseColor("#79A2AF"));
            set1.setCircleColor(Color.WHITE);

            set1.setFillAlpha(65);
            set1.setDrawValues(false);
            set1.setFillColor(Color.parseColor("#79A2AF"));
            set1.setDrawCircleHole(false);
            set1.setHighLightColor(Color.parseColor("#79A2AF"));
            set1.setValueTextColor(Color.WHITE);

            // set the filled area
            set1.setDrawFilled(true);
            set1.setFillFormatter(new IFillFormatter() {
                @Override
                public float getFillLinePosition(ILineDataSet dataSet, LineDataProvider dataProvider) {
                    return mLineChart.getAxisLeft().getAxisMinimum();
                }
            });

            ArrayList<ILineDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1); // add the data sets

            // create a data object with the data sets
            LineData data = new LineData(dataSets);

            // set data
            mLineChart.setData(data);
        }
    }
}