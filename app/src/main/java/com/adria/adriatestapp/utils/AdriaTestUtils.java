package com.adria.adriatestapp.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by LAKCHOUCH NAOUFAL on 20/08/20.
 */

@SuppressWarnings({"ALL", "unused"})
public class AdriaTestUtils {

    private final SimpleDateFormat simpleDateFormatProgramme = new SimpleDateFormat("yyyy-MM-dd", Locale.UK);
    private Calendar calendar;

    private static final String TAG = "AdriaTestUtils";

    public AdriaTestUtils() {}

    public Calendar getCalendar() {
        if (calendar == null) {
            calendar = Calendar.getInstance();
        }
        return (Calendar) calendar.clone();
    }

    public void resetCalendar() {
        calendar = Calendar.getInstance();
    }

    /** This method gets the current date
     * which is used as EndDate for the Exchange Rate History
     */
    public String getEndDate() {
        return simpleDateFormatProgramme.format(getCalendar().getTime());
    }

    /** This method gets the StartDate of Exchange Rate History
     * which is two months prior to the current date
     */
    public String getStartDate() {
        Calendar pastCalendar = getCalendar();
        pastCalendar.add(Calendar.MONTH, -2);
        return simpleDateFormatProgramme.format(pastCalendar.getTime());
    }

   /** This method gets the list of all the dates between a StartDate and an EndDate
    * in our case the difference between this two dates is 2 months
    */
    public List<String> getDatesList(){

        Date endDate = getCalendar().getTime();

        List<String> dates = new ArrayList<>();
        Calendar calendar = new GregorianCalendar();

        calendar.add(Calendar.MONTH, -2);
        Date startDate = calendar.getTime();
        calendar.setTime(startDate);

        while (calendar.getTime().before(endDate))
        {
            Date result = calendar.getTime();
            dates.add(simpleDateFormatProgramme.format(result));
            calendar.add(Calendar.DATE, 1);
        }
        return dates;
    }

    /**This method is used to check if the user is connected to WIFI network
     **/
    public boolean isConnectedToWiFi(Context context){
        ConnectivityManager connManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connManager.getActiveNetworkInfo();
        // connected to wifi
        return activeNetwork != null && activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;
    }

    /**This method is used to check if the user is connected to Mobile network
     **/
    public boolean isConnectedToMobile(Context context){
        ConnectivityManager connManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connManager.getActiveNetworkInfo();
        // connected to mobile data
        return activeNetwork != null && activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE;
    }

    /**This method is used to check if the user is connected to internet or not
     **/
    public boolean isNetworkConnected(Context context) {
        ConnectivityManager connManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connManager.getActiveNetworkInfo();

        return activeNetwork != null && activeNetwork.isConnected();
    }

    /**This method is used to connect to the internet and to get Json
     * from different Json WebServices
     * */
    public  String readFromUrl(String url) throws IOException {
        Log.v(TAG, url);

        StringBuilder manifest = new StringBuilder();
        InputStream in;
        URL mURL = new URL(url);
        HttpURLConnection urlConnection = (HttpURLConnection) mURL.openConnection();

        try {
            in = new BufferedInputStream(urlConnection.getInputStream());
            BufferedReader rd = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8), 8);
            String cur;
            try {
                while((cur = rd.readLine()) != null) {
                       manifest.append(cur);
                     }
                in.close();
            } catch (IOException e1) {
                Log.d(TAG, "Failed reading url " + url, e1);
            }
            urlConnection.disconnect();
        }catch (Exception e1) {
            Log.d(TAG, "Failed reading url " + url, e1);
        }finally {
            urlConnection.disconnect();
        }

        return manifest.toString();
    }

    /** Using AssetManager to get a bitmap from a file
     */
    public Bitmap getBitmapFromAsset(Context context, String filePath) {
        AssetManager assetManager = context.getAssets();

        InputStream istr;
        Bitmap bitmap = null;
        try {
            istr = assetManager.open(filePath);
            bitmap = BitmapFactory.decodeStream(istr);
        } catch (IOException e) {
            // handle exception
        }

        return bitmap;
    }
}
