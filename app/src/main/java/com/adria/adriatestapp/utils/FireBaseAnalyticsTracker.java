package com.adria.adriatestapp.utils;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.google.firebase.analytics.FirebaseAnalytics;

/**
 * Created by LAKCHOUCH NAOUFAL on 21/08/20.
 */

public class FireBaseAnalyticsTracker {

    private static FirebaseAnalytics tracker;

    private static synchronized void initTracker(Context ctx) {
        if (tracker == null) {
            // Obtain the FirebaseAnalytics instance.
            tracker = FirebaseAnalytics.getInstance(ctx.getApplicationContext());
        }
    }

    public static void trackScreen(Context ctx, String screen) {
        trackScreen(ctx, screen, null);
    }

    // Tracking Screen method
    public static void trackScreen(Context ctx, String screen, String additionnal) {
        if (tracker == null) {
            initTracker(ctx);
        }

        Bundle bundle = new Bundle();
        String screenName = screen + (additionnal != null ? " " + additionnal : "");
        bundle.putString("Screen", screenName);
        tracker.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW , bundle);

        Log.d("FireBaseAnalytics", "TrackingView-" + screenName);
    }
}
