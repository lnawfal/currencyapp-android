package com.adria.adriatestapp.utils;

import android.content.Context;

import com.adria.adriatestapp.R;

/**
 * Created by LAKCHOUCH NAOUFAL on 21/08/20.
 */

public class UIUtils {

    public UIUtils() {}

    public boolean isTablet(Context context) {
        return context.getResources().getBoolean(R.bool.is_tablet);
    }
}
